import React from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import { DBAPI } from '../Api';
import { GalleryImg } from '../elements/elements';
import Gallery from 'react-native-image-gallery';

import {
  ActivityIndicator,
  StyleSheet,
  Text,
  View,
  Dimensions,
  StatusBar,
  Image,
  Platform,
  TouchableOpacity,
  TouchableHighlight,
  AsyncStorage,
  ScrollView
} from 'react-native';

const HEIGHT = Dimensions.get('window').height;
const WIDTH = Dimensions.get('window').width;
const contentHeight = HEIGHT - 75;
const PREFIX = (Platform.OS == 'ios') ? 'ios-' : 'md-';
const Touch = (Platform.OS == 'ios') ? TouchableOpacity : TouchableHighlight;
const StatusBarD = (Platform.OS == 'ios') ?  <View style={{height: 25, backgroundColor: '#F26131'}}>
          <StatusBar barStyle='light-content' />
        </View> : null;


export default class IntHotelScreen extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      baseUrl: 'http://localhost:3000/',
      imgsGall: [{ source: { uri: 'https://i.imgur.com/XP2BE7q.jpg' } }],
      hotel: {
        images: []
      },
      loading: true,
      selectIndex: 0,
      showGall: false
    };
  };

  getHotel () {
    const { params } = this.props.navigation.state;
    let RC = this, imgsGall = [];;
    var url = RC.state.baseUrl + 'hotel';
    // Get all hotels
    AsyncStorage.getItem('LocalUrl', (err, result) => {
      if (result) {
        RC.setState({baseUrl: result});
        url = result + 'hotel';
      }
      DBAPI(url, 'POST', {id: params.id}).then((response) => {
        RC.setState({
          hotel: response,
          loading: false
        });
        setTimeout(() => {
          for (let pp = 0; pp < response.images.length; pp++) {
            imgsGall.push({
              source: { uri: response.images[pp].uri },
            });
          }
          RC.setState({
            imgsGall: imgsGall
          });
        }, 200);
      }).catch((response) => {
        // console.warn(response);
        RC.setState({
          loading: false
        });
      });
    });
  };

  componentWillMount () {
    this.getHotel();
  };

  render() {
    const { goBack } = this.props.navigation;
    let STATES = this.state;
    let LAB = STATES.hotel.images[STATES.selectIndex]  ? STATES.hotel.images[STATES.selectIndex] : {};
    // List images
    const listImages = this.state.hotel.images.map((image, key) => {
      const index = key + 1;
      return (<GalleryImg key={index}  label={this.state.hotel.name} image={image.uri} function={() => {this.setState({showGall: true, selectIndex: key})}} /> );
    });
    let contents = this.state.loading ? <View style={styles.loader}>
      <ActivityIndicator animating={true} size={'large'}/>
    </View> : <ScrollView style={styles.info}>
          <Text style={{color: '#4D036C', fontWeight: 'bold', fontSize: 15}}>Nombre: {this.state.hotel.name}</Text>
          <Text style={{marginTop: 10, fontWeight: 'bold'}}>Ciudad: {this.state.hotel.cityVal}</Text>
          <View style={{flexDirection: 'row', marginTop: 15, backgroundColor: 'white', paddingBottom: 4}}>
            <Icon name={this.state.hotel.stars >= 1 ? 'ios-star' : 'ios-star-outline'} size={25} style={{color: this.state.hotel.stars >= 1  ? '#F26131' : '#9B9B9B'}} />
            <Icon name={this.state.hotel.stars >= 2 ? 'ios-star' : 'ios-star-outline'} size={25} style={{color: this.state.hotel.stars >= 1  ? '#F26131' : '#9B9B9B'}} />
            <Icon name={this.state.hotel.stars >= 3 ? 'ios-star' : 'ios-star-outline'} size={25} style={{color: this.state.hotel.stars >= 1  ? '#F26131' : '#9B9B9B'}} />
            <Icon name={this.state.hotel.stars >= 4 ? 'ios-star' : 'ios-star-outline'} size={25} style={{color: this.state.hotel.stars >= 1  ? '#F26131' : '#9B9B9B'}} />
            <Icon name={this.state.hotel.stars >= 5 ? 'ios-star' : 'ios-star-outline'} size={25} style={{color: this.state.hotel.stars >= 1  ? '#F26131' : '#9B9B9B'}} />
            <View style={styles.price}>
              <Text style={styles.labelprice}>Precio: USD ${this.state.hotel.priceV}</Text>
            </View>
          </View>
          <Text style={{marginTop: 10}}>Lista de imágenes: </Text>
          {listImages}

          <View style={{height: 80}}></View>
        </ScrollView>;
    return (
      <View style={{flex: 1, backgroundColor: 'white'}}>

        {StatusBarD} 
      
        <View style={styles.header}>
          <Touch activeOpacity={0.4} underlayColor = 'transparent' style={{ position: 'absolute', top: 10, left: 20}} onPress={() => {goBack()}}>
            <Icon name={PREFIX + 'arrow-back'} size={30} style={{color: 'white'}} />
          </Touch>
          <Text style={{color: 'white', position: 'absolute', fontSize: 20, top: 10, textAlign: 'center', width: WIDTH - 110}}>Detalle del hotel</Text>
        </View>

        {contents}

        <View style={[styles.contGall, {height: (this.state.showGall ? HEIGHT : 0), overflow: 'hidden'}]}>
          <Gallery
            style={{  flex: 1, backgroundColor: 'black' }}
            images={[(STATES.imgsGall[STATES.selectIndex] ? STATES.imgsGall[STATES.selectIndex] : null)]}
          />
          <Touch  activeOpacity={0.4} underlayColor = 'transparent'  onPress={() => {this.setState({showGall: false})}} style={styles.closeBt}>
            <Icon name='md-close' color='#fff' size={30}  style={{color: 'white'}} />
          </Touch>
        </View>

        
      </View>
    );
  }
};

const styles = StyleSheet.create({
  header: {
    height: 50,
    backgroundColor: '#F26131',
    alignItems: 'center'
  },
  loader: {
    height: HEIGHT - 100,
    alignContent: 'center',
    justifyContent: 'center'
  },
  info: {
    padding: 15
  },
  price: {
    position: 'absolute', 
    right: 0,
    height: 28,
    width: 150,
    paddingTop: 3
  },
  labelprice: {
    color: '#9B9B9B', 
    fontWeight: 'bold', 
    textAlign: 'center', 
    backgroundColor: 'transparent',
    fontSize: 13
  },
  contGall: {
    position: 'absolute',
    width: WIDTH,
    zIndex: 5,
    top: 0,
    left: 0
  },
  closeBt: {
    position: 'absolute',
    backgroundColor: 'transparent',
    width: 60,
    height: 60,
    zIndex: 6,
    top: 0,
    right: 0,
    alignItems: 'center',
    justifyContent: 'center'
  }
});