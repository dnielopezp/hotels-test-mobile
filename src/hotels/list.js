import React from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import { Card } from '../elements/elements';
import { DBAPI } from '../Api';
import SearchInput, { createFilter } from 'react-native-search-filter';
const KEYS_TO_FILTERS = ['cityVal', 'name', 'priceV'];

import {
  ActivityIndicator,
  StyleSheet,
  Text,
  View,
  Platform,
  TouchableOpacity,
  TouchableHighlight,
  Dimensions,
  FlatList,
  StatusBar,
  Image,
  Modal,
  TextInput,
  AsyncStorage
} from 'react-native';

const HEIGHT = Dimensions.get('window').height;
const WIDTH = Dimensions.get('window').width;
const contentHeight = HEIGHT - 75;
const PREFIX = (Platform.OS == 'ios') ? 'ios-' : 'md-';
const Touch = (Platform.OS == 'ios') ? TouchableOpacity : TouchableHighlight;
const StatusBarD = (Platform.OS == 'ios') ?  <View style={{height: 25, backgroundColor: '#F26131'}}>
          <StatusBar barStyle='light-content' />
        </View> : null;


export default class ListHotels extends React.Component {

  constructor (props) {
    super(props);
    this.state = {
      loading: true,
      limit: 10,
      hotelsTotal: [],
      hotels: [],
      baseUrl: 'http://localhost:3000/',
      modalVisible: false,
      searchTerm: ''
    };
  };

  _keyExtractor = (item, key) => {
    return (key);
  };


  thresholdOn = () => {
    let RC = this;
    let hotels = [], sizetTotal = RC.state.hotelsTotal.length;
    let limit = RC.state.limit + 10;
    let resta = sizetTotal - limit;
    if (resta >= -10) {
      for (let i = (RC.state.limit); i < limit; i++) {
        if ((sizetTotal - 1 - i) >= 0) {
          hotels.push(RC.state.hotelsTotal[(sizetTotal - 1 - i)]);
        }
      }
      RC.setState({
        hotels: [...RC.state.hotels, ...hotels],
        limit: limit
      });
    }
  };

  showModal () {
    this.setState({modalVisible: true});
  };

  newUrl () {
    var RN = this;
    AsyncStorage.setItem('LocalUrl', RN.state.baseUrl, (err) => {
      RN.setState({modalVisible: false});
      RN.getHotels();
    });
  };

  getHotels () {
    let RC = this;
    var url = RC.state.baseUrl + 'hotels';
    RC.setState({loading: true});
    // Get all hotels
    AsyncStorage.getItem('LocalUrl', (err, result) => {
      if (result) {
        RC.setState({baseUrl: result});
        url = result + 'hotels';
      }
      DBAPI(url, 'GET', {}).then((response) => {
        let hotels = [], sizetTotal = response.length;
        for (let i = 0; i < RC.state.limit; i++) {
          // Llenar primeros 10 con los últimos 10 del array total
          if ((sizetTotal - 1 - i) >= 0) {
            hotels.push(response[(sizetTotal - 1 - i)]);
          }
        }
        RC.setState({
          hotelsTotal: response,
          hotels: hotels,
          loading: false
        });
      }).catch((response) => {
        // console.warn(response);
        RC.setState({
          loading: false
        });
      });
    });
  };

  componentWillMount() {
    let RC = this;
    RC.getHotels();
  };

  render() {
    const filteredEmails = this.state.hotels.filter(createFilter(this.state.searchTerm, KEYS_TO_FILTERS));
    let contents = this.state.loading ? <View style={styles.loader}>
      <ActivityIndicator animating={true} size={'large'}/>
    </View> : <FlatList
          data={filteredEmails}
          keyExtractor={this._keyExtractor}
          refreshing={false}
          onEndReached={this.thresholdOn}
          onEndThreshold={500}
          onRefresh={this.getHotels.bind(this)}
          renderItem={({item}) =>
            <Card hashId={item['hash_id']} image={item.images[0]} category={'Ciudad: ' + item.cityVal}  title={item.name} date={'Cantidad de fotos: ' + item.images.length}  stars={item.stars} price={item.priceV} allItem={item} navigation={this.props.navigation}/>
          }
        />;
    return (
      <View style={{flex: 1, backgroundColor: 'white'}}>

        <Modal
          animationType={"slide"}
          transparent={false}
          visible={this.state.modalVisible}
          onRequestClose={() => {this.setState({modalVisible: false})}}
          >
          {StatusBarD}
          <View style={styles.containerModal}>
            <View style={[styles.header, {width: WIDTH, position: 'absolute', top: 0, left: 0, zIndex: 10}]}>
              <Touch activeOpacity={0.4} underlayColor = 'transparent' style={{ top: 10, left: 20, position: 'absolute'}} onPress={() => {this.setState({modalVisible: false})}}>
                <Icon name={PREFIX + 'arrow-back'} size={30} style={{color: 'white'}} />
              </Touch>
              <Text style={{color: 'white', position: 'absolute', fontSize: 20, top: 10}}>Configuración</Text>
            </View>
            <Text style={{width: window.width * 0.8, textAlign: 'center'}}>Ingresa la dirección URL del servidor (Ejemplo: http://192.168.0.10:3000/)</Text>
            <TextInput underlineColorAndroid='transparent' onChangeText={(text) => this.setState({baseUrl: text})}
            value={this.state.baseUrl} placeholder={'Escribe la URL de tu servidor.'} style={styles.input} multiline = {false} editable = {true}/>
            <Touch activeOpacity={0.4} underlayColor = '#F26131' style={[styles.button, { backgroundColor: '#F26131', width: WIDTH * 0.8, marginTop: 10 }]} onPress={this.newUrl.bind(this)}>
              <Text  style={{color: 'white', fontWeight: 'bold', textAlign: 'center'}}>Listo</Text>
            </Touch>
          </View>
        </Modal>

        {StatusBarD} 
      
        <View style={styles.header}>
          <Touch activeOpacity={0.4} underlayColor = 'transparent' style={{ position: 'absolute', top: 10, right: 20}} onPress={() => {this.setState({modalVisible: true})}}>
            <Icon name={PREFIX + 'settings'} size={30} style={{color: 'white'}} />
          </Touch>
          <Text style={{color: 'white', position: 'absolute', fontSize: 20, top: 10, textAlign: 'center', width: WIDTH - 110}}>Lista de Hoteles</Text>
        </View>
        <View style={styles.search}>
          <SearchInput 
            onChangeText={(term) => { this.setState({ searchTerm: term }) }} 
            style={styles.searchInput}
            placeholder="Busca por el nombre, ciudad o precio del hotel ..."
            />
        </View>

        {(this.state.hotels.length === 0 && !this.state.loading) ? <View style={{height: HEIGHT - 50, width: WIDTH, position: 'absolute', top: 50, left: 0, backgroundColor: 'transparent', alignItems: 'center', justifyContent: 'center', zIndex: 5}}>
          <Text style={{color: 'black', fontSize: 16, textAlign: 'center', padding: 20}}>Bienvenido/a a Hotel List, lastimosamente no hay hoteles por mostrar o pueda que sea un error del servidor. Verifique la URL del servidor dando click en el icono de Configuración.</Text>
        </View> : null}
        
        {contents}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    height: 50,
    backgroundColor: '#F26131',
    alignItems: 'center'
  },
  search: {
    height: 50
  },
  searchInput:{
    padding: 10,
    borderColor: '#CCC',
    borderWidth: 1
  },
  loader: {
    height: HEIGHT - 100,
    alignContent: 'center',
    justifyContent: 'center'
  },
  button: {
    width: WIDTH * 0.8,
    height: 40,
    borderWidth: 0,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 2,
    marginTop: 15
  },
  containerModal: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  input: {
    width: WIDTH * 0.8,
    height: 50,
    borderBottomWidth: 2,
    borderColor: 'gray',
    marginTop: 20
  },
});
