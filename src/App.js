import React from 'react';

import { 
  AppRegistry
} from 'react-native';
import { 
  StackNavigator
} from 'react-navigation';


// Import components

import ListHotels from './hotels/list';
import IntHotelScreen from './hotels/intHotel';

const testHotels = StackNavigator(
  {
    List: { 
      screen:  ListHotels, 
      navigationOptions: {
        header: null
      } 
    },
    IntHotel: { 
      screen:  IntHotelScreen, 
      navigationOptions: {
        header: null
      } 
    },
  }, 
  {
    cardStyle : {
      backgroundColor: 'white'
    }
  }
);

AppRegistry.registerComponent('testHotels', () => testHotels);
