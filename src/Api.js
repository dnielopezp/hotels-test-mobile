export const DBAPI = (url, method, body) => {
  return new Promise ((res, rej) => {
    if (url && method) {
      if (!body.id) {
        Promise.all([url, method]).then(values => {
          fetch(values[0], {
              method: values[1],
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
              }
            }).then((response) => {
              let body = JSON.parse(response._bodyInit);
              res(body.data);
            }).catch((response) => {
              rej(response);
            });
        }).catch((err) => {
          // console.warn('Error URL');
          rej(err);
        });
      }
      else {
        Promise.all([url, method]).then(values => {
          fetch(values[0], {
              method: values[1],
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
              },
              body: JSON.stringify({
                id: body.id
              })
            }).then((response) => {
              let body = JSON.parse(response._bodyInit);
              res(body.data);
            }).catch((response) => {
              rej(response);
            });
        }).catch((err) => {
          // console.warn('Error URL');
          rej(err);
        });
      }
    }
    else {
      rej();
    }
  });
};
