import React from 'react';

import { View, StyleSheet, Image, Platform, TouchableOpacity, TouchableHighlight, Dimensions } from 'react-native';

const WIDTH = Dimensions.get('window').width;
const Touch = (Platform.OS == 'ios') ? TouchableOpacity : TouchableHighlight;

export class _GalleryImg extends React.Component {
  render() {
    return (
      <View style={styles.imageCont}>
        <Image style={styles.imageIn} source={{uri: this.props.image}} />
        <Touch activeOpacity={0.6} underlayColor={'transparent'}  onPress={this.props.function} style={[styles.touch, { backgroundColor: 'transparent'}]}><View/></Touch>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  imageCont: {
    width: WIDTH - 34,
    height: 195,
    marginTop: 10,
    alignItems: 'center',
    justifyContent: 'center'
  },
  imageIn: {
    width: WIDTH - 34,
    height: 195,
    zIndex: 1
  },
  touch: {
    position: 'absolute',
    top: 0,
    left: 0,
    zIndex: 7,
    width: WIDTH - 34,
    height: 195,
    opacity: 0
  }
});
