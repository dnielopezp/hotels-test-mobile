import React from 'react';

import {
  Text,
  View,
  StyleSheet,
  Dimensions,
  Image,
  TouchableOpacity,
  TouchableHighlight,
  Platform,
  Alert,
  Linking,
  AsyncStorage,
  ActivityIndicator,
  Share
} from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import { DBAPI } from '../Api';


const window = Dimensions.get('window');
const Touch = (Platform.OS == 'ios') ? TouchableOpacity : TouchableHighlight;

export class _Card extends React.Component {
  constructor (props) {
    super(props);
    this.state = {}
  };

  setNativeProps = (nativeProps) => {
    this._root.setNativeProps(nativeProps);
  };


  componentWillMount () {};
  
  render() {
    let image = this.props.image ? <View style={styles.cardContent}>
      <Image style={styles.imageC} source={{uri: this.props.image.uri}} />
      <Text style={styles.category}>{this.props.category}</Text>
    </View> : <View style={styles.cardContent}>
      <Image style={styles.imageC} source={require('../Assets/no-photo.jpg')} />
      <Text style={styles.category}>{this.props.category}</Text>
    </View> ;
    var RC = this;
    return (
      <View style={styles.card} ref={component => this._root = component} {...this.props}>
        {image}
        <View style={styles.cardFooter}>
          <Text style={{color: '#4D036C', fontWeight: 'bold', fontSize: 15}}>{this.props.title}</Text>
          <Text style={{marginTop: 10, fontWeight: 'bold'}}>{this.props.date}</Text>
          <View style={{flexDirection: 'row', marginTop: 15, backgroundColor: 'white', paddingBottom: 4}}>
            <Icon name={this.props.stars >= 1 ? 'ios-star' : 'ios-star-outline'} size={25} style={{color: this.props.stars >= 1  ? '#F26131' : '#9B9B9B'}} />
            <Icon name={this.props.stars >= 2 ? 'ios-star' : 'ios-star-outline'} size={25} style={{color: this.props.stars >= 1  ? '#F26131' : '#9B9B9B'}} />
            <Icon name={this.props.stars >= 3 ? 'ios-star' : 'ios-star-outline'} size={25} style={{color: this.props.stars >= 1  ? '#F26131' : '#9B9B9B'}} />
            <Icon name={this.props.stars >= 4 ? 'ios-star' : 'ios-star-outline'} size={25} style={{color: this.props.stars >= 1  ? '#F26131' : '#9B9B9B'}} />
            <Icon name={this.props.stars >= 5 ? 'ios-star' : 'ios-star-outline'} size={25} style={{color: this.props.stars >= 1  ? '#F26131' : '#9B9B9B'}} />
            <View style={styles.price}>
              <Text style={styles.labelprice}>Precio: USD ${this.props.price}</Text>
            </View>
          </View>
        </View>
        <Touch activeOpacity={0.4} underlayColor = 'rgba(0, 0, 0, 0.1)' style={styles.hoverB} onPress={() => {this.props.navigation.navigate('IntHotel', {id: this.props.allItem._id})}}>
          <Text></Text>
        </Touch>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  hoverB: {
    backgroundColor: 'transparent',
    position: 'absolute',
    zIndex: 1000,
    width: window.width - 32,
    height: 320,
    top: 0,
    left: 0
  },
  card: {
    width: window.width - 32,
    margin: 16,
    marginBottom: 0,
    backgroundColor: 'white',
    borderRadius: 2,
    borderColor: '#F2F2F2',
    borderWidth: 1,
    overflow: 'hidden'
  },
  // Image content
  cardContent: {
    height: 195
  },
  imageC: {
    width: window.width - 34,
    height: 195
  },
  category: {
    position: 'absolute',
    bottom: 15,
    left: 15,
    backgroundColor: '#00E0CD',
    color: 'white',
    padding: 5,
    fontWeight: 'bold',
  },
  // Footer
  cardFooter: {
    padding: 15
  },
  price: {
    position: 'absolute', 
    right: 0,
    height: 28,
    width: 150,
    paddingTop: 3
  },
  labelprice: {
    color: '#9B9B9B', 
    fontWeight: 'bold', 
    textAlign: 'center', 
    backgroundColor: 'transparent',
    fontSize: 13
  }
});
