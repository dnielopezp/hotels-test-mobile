var elements = {};
// Import elements
elements.card = require('./_card.js');
elements.galleryImg = require('./_galleryImg.js');
// Export elements
export const Card = elements.card._Card;
export const GalleryImg = elements.galleryImg._GalleryImg;
