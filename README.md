# Hotels Test App for Almundo.com

## Starting serve

1. Clone this repo in your localhost
2. Run `npm install` or `yarn install`
3. Start the dev server using `react-native start` in other tab in your terminal
4. Open one tab in your terminal and run `react-native run-android` or `react-native run-ios` (Remmember that react native use emulator of iOS and Android, so you should be sure that you have installed all libraries and SDKs) Follow the documentation of [https://facebook.github.io/react-native/docs/](https://facebook.github.io/react-native/docs/)
5. If you want use the app in device install the apk in Android (`./android/app/build/outputs/apk/app-debug.apk` or connect the device via USB and run `react-native run-android`) or use XCODE to install in iOS device

## Author
Daniel López
dniel_lopez@hotmail.com
Bogota, Colombia
